import './styles/index.less';
import './styles/main.less';

import Education from './js/education.js';
import Person from './js/person.js';

//TODO: change to jquery
import $ from 'Jquery';

function fetchData(url) {
  return fetch(url).then(response => response.json());
}

const URL = 'http://localhost:3000/person';
fetchData(URL)
  .then(result => {
    //TODO: bad naming: kamil should be person
    const person = new Person(
      result.name,
      result.age,
      result.description,
      result.educations
    );
    $('#introduce').append(`${person.introduce()}`);
    //TODO: bad naming: about-me
    $('#description').append(`${person.description}`);
    let educations = [];
    //TODO: ES6 syntax will simplify for-each
    for (let i = 0; i < result.educations.length; i++) {
      educations.push(
        new Education(
          result.educations[i].year,
          result.educations[i].title,
          result.educations[i].description
        )
      );
    }
    //TODO: bad design(html & js): if there are 1000000 year, you will add 100000 lines?
    for (let i = 0; i < educations.length; i++) {
      $(`#education${i}`).append(`<article class="education-content">
          <p class="year year-font-size font-bold">${educations[i].year}</p>
          <div class="education-info">
            <p class="summary font-bold">${educations[i].summary}</p>
            <p class="description">${educations[i].description}</p>
          </div>
        </article>`);
    }
  })
  .catch(error => {
    console.error(error);
  });

//TODO: this file does too many things, think about file responsibility
//TODO: Well: 小步提交；提交与tasking很对应；Tasking基本基于业务
