export default class Education {
  constructor(year, summary, description) {
    this.year = year;
    this.summary = summary;
    this.description = description;
  }
}
