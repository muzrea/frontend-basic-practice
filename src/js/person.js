export default class Person {
  constructor(name, age, description, educations) {
    this.name = name;
    this.age = age;
    this.description = description;
    this.educations = educations;
    //TODO: education should be an attribute of Person
  }

  introduce() {
    return `MY NAME IS ${this.name} ${this.age}YO AND THIS IS MY RESUME/CV`;
  }
}
